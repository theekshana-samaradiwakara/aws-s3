#!/bin/bash

imageBucket="s3://theekshana-test-image"

accountsBucket="s3://theekshana-test-accounts"

APP_ENVIRONMENT=""

#select image bucket environment
#eg:- s3://<env>-images.cake.net
if [[ ! -z "$APP_ENVIRONMENT" ]]; then
    echo "---------APP_ENVIRONMENT: ${APP_ENVIRONMENT}"
    imageBucket="s3://theekshana-test-${APP_ENVIRONMENT}-image-bucket"
else
    APP_ENVIRONMENT="other"
  	echo "---------APP_ENVIRONMENT is not set. Syncing image content to ${imageBucket} bucket..."
fi

#sync with image bucket
echo "---------S3 SYNC images/${APP_ENVIRONMENT} TO: ${imageBucket}" 

aws s3 sync "images/${APP_ENVIRONMENT}" "${imageBucket}" --exclude "*" --include "*.jpg" --include "*.jpeg" --include "*.png" --include "*.GIF" --include "*.woff2"  --include "*.eot" --include "*.ttf" --include "*.ico" --delete

#sync with account bucket
echo "---------S3 SYNC ${accountsFolder} TO: ${accountsBucket}"

aws s3 sync "accounts/" "${accountsBucket}" --exclude "*" --include "*.json" --delete
